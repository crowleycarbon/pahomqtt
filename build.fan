/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2016 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//   09 Jan 2016  John MacEnri  Creation
//
using build

class Build : BuildPod
{
  new make()
  {
    podName = "pahoMqtt"
    summary = "Eclipse.org PAHO MQTT Java Client Library Fantom Wrapper"
    meta    = ["org.name":     "Eclipse",
               "org.uri":      "https://www.eclipse.org/paho/clients/java/",
               "proj.name":    "Paho",
               "license.name": "EPL",
               "vcs.name": "Git",
               "vcs.commit.id": "COMMIT_ID",
               "vcs.commit.date": "COMMIT_DATE",
               "vcs.commit.user": "COMMIT_USER",
               "vcs.commit.comment": "COMMIT_COMMENT"               
               ]
	depends  = ["sys 1.0"]
    version = Version("1.1.0")
    resDirs = [`org.eclipse.paho.client.mqttv3-1.1.0.jar`]
  }
  
}
